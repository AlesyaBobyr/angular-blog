import {Environment} from './interface';

export const environment: Environment = {
  production: true,
  apiKey: 'AIzaSyBao33zAfFdBVODuDRyMYOk98p1d2mWyxU',
  fbDbUrl: 'https://angular-blog-8d4fc-default-rtdb.europe-west1.firebasedatabase.app'
};
