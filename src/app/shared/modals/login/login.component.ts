import {Component, HostListener, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  email: string;
  password: string;
  constructor(
    private dialogRef: MatDialogRef<LoginComponent>,
  ) {
  }

  ngOnInit(): void {
    /**
     * todo: create form with two FormControls, add validation to it
     * email => it is really email
     * password => minimal length 6 chars
     */
    this.form = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }
  }

  close(): void {
    this.dialogRef.close();
  }

}
