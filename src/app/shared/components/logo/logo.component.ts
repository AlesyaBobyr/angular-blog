import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent {
  @Input() size: 's' | 'm' | 'l' = 's';

  get iconSize(): number {
    switch (this.size) {
      case 's':
        return 43;
      case 'm':
        return 56;
      case 'l':
        return 60;
    }
  }

  get textSize(): number {
    switch (this.size) {
      case 's':
        return 18;
      case 'm':
        return 23;
      case 'l':
        return 27;
    }
  }
}
