import { Component, OnInit } from '@angular/core';
import { GiftsService } from '../../services/gifts/gifts.service';
import {Gift} from '../../services/gift.interface';


@Component({
  selector: 'app-gifts',
  templateUrl: './gifts.component.html',
  styleUrls: ['./gifts.component.scss']
})
export class GiftsComponent implements OnInit {
  typeGifts: string[];
  selectedGiftType: string;
  gifts: Gift[] = [];
  filteredGifts: Gift[];

  constructor(
    private giftsService: GiftsService
  ) {
  }

  ngOnInit(): void {
    this.giftsService.loadGiftTypes().subscribe(
      giftTypes => {
        console.log('giftTypes', giftTypes);
        this.typeGifts = giftTypes;
        this.selectedGiftType = giftTypes[0];
      }
    );

    this.giftsService.loadGifts().subscribe(
      gifts => {
        this.gifts = gifts;
        this.filteredGifts = gifts;
      }
    );
  }

  listenForModelChangeForTest(): void {
    if (this.selectedGiftType === 'All') {
      // If selected gift type is 'All' then we set to filtered gifts whole gifts array
      this.filteredGifts = this.gifts;
    }
    else {
      // But if you choose gift type than should filter all gifts by type
      this.filteredGifts = this.gifts.filter(gift => gift.params.type === this.selectedGiftType);
    }
    console.log('selectedGiftType', this.selectedGiftType);
  }
}

