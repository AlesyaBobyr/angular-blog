import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { QuillModule } from 'ngx-quill';
import { LoginComponent } from './modals/login/login.component';
import { MaterialSharedModule } from '../modules/material-shared/material-shared.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LogoComponent } from './components/logo/logo.component';

@NgModule({
  imports: [
    HttpClientModule,
    QuillModule.forRoot(),
    MaterialSharedModule,
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    HttpClientModule,
    QuillModule,
    LogoComponent
  ],
  declarations: [
    LoginComponent,
    LogoComponent
  ]
})
export class SharedModule {
}
