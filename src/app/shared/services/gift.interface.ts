export interface Gift {
  id?: string;
  title: string;
  subtitle: string;
  category: string;
  amount: number;
  currency: string;
  params: {
    type: string;
    image: string;
  };
}
