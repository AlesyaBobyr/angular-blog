import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GiftsService {
  constructor(private httpClient: HttpClient) { }

  loadGifts(): Observable<any> {
    return this.httpClient.get('assets/data/gifts.json');
  }

  loadGiftTypes(): Observable<string[]> {
    const typeGifts = ['All', 'Christmas', 'Drink', 'Flowers', 'Food', 'Jewelry', 'Lifestyle', 'Romantic', 'Sport', 'Toys', 'Transport'];
    return of(typeGifts);
  }
}
