import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostsService } from '../../shared/posts.service';
import { Post } from '../../shared/interfaces';
import { ReplaySubject, Subscription } from 'rxjs';
import { AlertService } from '../shared/services/alert.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  pSub: Subscription;
  dSub: Subscription;
  searchStr = '';
  destroy$ = new ReplaySubject(1);

  constructor(
    private postsService: PostsService,
    private alert: AlertService
  ) {
  }

  ngOnInit(): void {
    this.postsService.getAll()
      .pipe(takeUntil(this.destroy$))
      .subscribe(posts => {
        this.posts = posts;
      });
  }

  remove(id: string): void {
    this.postsService.remove(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.posts = this.posts.filter(post => post.id !== id);
        this.alert.warning('Пост был удален');
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
